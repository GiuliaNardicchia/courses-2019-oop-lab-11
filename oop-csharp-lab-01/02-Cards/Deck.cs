﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
            this.cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length];
        }

        public Card this[ItalianSeed seed, ItalianValue value]
        {
            get
            {
                foreach (Card card in cards)
                {
                    if (card.Seed.Equals(seed.ToString()) && card.Value.Equals(value.ToString()))
                    {
                        return card;
                    }
                }
                return null;
            }
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Suggerimento - usare gli array.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            int i = 0;
            ItalianSeed[] seeds = (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed));
            ItalianValue[] values = (ItalianValue[])Enum.GetValues(typeof(ItalianValue));
            foreach (ItalianSeed seed in seeds)
            {
                foreach (ItalianValue value in values)
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
             foreach(Card card in cards)
             {
                Console.WriteLine(card.ToString());
             }
        }

    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}